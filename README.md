## NXCore_Icom [![CircleCI](https://circleci.com/gh/rthoelen/NXCore_Icom.svg?style=shield)](https://circleci.com/gh/rthoelen/NXCore_Icom)

# NXCore Manager for Icom Repeaters

To build with Debian, Raspbian, Ubuntu:

Run the build_debian.sh script. This will install dependencies and generate the binary.

